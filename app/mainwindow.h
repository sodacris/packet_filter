#pragma once
#include <QtWidgets>
#include "lib.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void processMessage(Message message);
private:
    void loadPlugins();

    Ui::MainWindow* ui;
    CaptureProcess process;
};
