#include "lib.h"
#include "plugindef.h"
#include <minwindef.h>
#ifdef WINNT
#include "sysinfoapi.h"
#include "winnt.h"
#endif

bool IsSupportedOS()
{
#ifdef WINNT
    OSVERSIONINFO osvi;
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&osvi);

    bool isWindows7orLater = ((osvi.dwMajorVersion > 6) || ((osvi.dwMajorVersion == 6) && (osvi.dwMinorVersion >= 1)));

    if (isWindows7orLater) {
        return true;
    } else {
        return false;
    }
#else
    return true;
#endif
}

CaptureProcess::CaptureProcess(QObject* parent)
    : QObject(parent)
{
    connect(&process, &QProcess::readyRead,
        this, &CaptureProcess::onOutput);
}

void CaptureProcess::start()
{
    if (!IsSupportedOS())
        return;
#ifdef WINNT
    process.start("C:/Program Files/Wireshark/tshark.exe",
        {
            "-nnxi6",
            "-P",
            "-Yudp && (eth.dst[0] & 1) && !ssdp",
            "-Tek",
        });
#else
    process.start("tcpdump", { "-nn", "-x" });
#endif
}

void CaptureProcess::onOutput()
{
    buffer += process.readAllStandardOutput();
#ifdef WINNT
    QStringList jsons;
    auto start = 0, iter = 0;
    for (; iter < buffer.size(); iter++) {
        if (buffer.at(iter) == '\n' && iter != 0 && buffer.at(iter - 1) == '\r') {
            jsons << buffer.mid(start, iter - start).trimmed();
            start = iter;
        }
    }
    buffer = buffer.mid(start);
    for (auto& json : qAsConst(jsons)) {
        QJsonDocument doc = QJsonDocument::fromJson(json.toLatin1());
        auto obj = doc.object();
        if (!obj.contains("index")) {
            auto destination = obj.value("destination").toString();
            auto data = obj.value("layers").toObject().value("data_raw").toString();
            auto udpHeader = obj.value("layers").toObject().value("udp_raw").toString();
            bool ok { false };
            auto srcPort = udpHeader.midRef(0, 4).toUInt(&ok, 16);
            auto destPort = udpHeader.midRef(4, 4).toUInt(&ok, 16);

            Message message;
            message.destIP = destination;
            message.data = data;
            message.destPort = destPort;
            emit sigMessage(message);
        }
    }
#endif
}
