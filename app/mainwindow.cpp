#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&process, &CaptureProcess::sigMessage,
        this, &MainWindow::processMessage);
    process.start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::processMessage(Message message)
{
    qDebug() << message.destIP << message.destPort;
}
