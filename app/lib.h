#pragma once
#include "plugindef.h"

bool IsSupportedOS();

class CaptureProcess : public QObject
{
    Q_OBJECT
public:
    CaptureProcess(QObject* parent = nullptr);
    void start();
signals:
    void sigMessage(Message message);
private slots:
    void onOutput();
private:
    QString buffer;
    QProcess process;
};
