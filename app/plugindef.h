#pragma once
#include <QtCore>

struct Message {
    QString destIP;
    int destPort;
    QString data;
};

class FilterInterface
{
public:
    virtual ~FilterInterface();
    virtual bool filter();
    virtual QString name();
    virtual void onMessage(Message message);
};
