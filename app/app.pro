TEMPLATE = app
QT += core widgets

TARGET = app

HEADERS += mainwindow.h \
           plugindef.h \
           lib.h \

SOURCES += mainwindow.cpp \
           main.cpp \
           lib.cpp \

FORMS += mainwindow.ui
